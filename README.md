[![](https://gitlab.com/nael_d/encrypia/-/raw/master/logo-main.png)](https://gitlab.com/nael_d/encrypia)

Securely encrypt and decrypt text data with our custom PHP-based encryption class.

## Key Features
- **Blind & Unblind**: Encrypt and decrypt text using a unique key you provide.
- **PHP Compatibility**: Works with PHP versions **7.0.0** and above.

## Installation
Installing Encrypia is available through Composer:
```console
composer install nael_d/encrypia
```
Alternatively, you can [download latest Encrypia version](https://gitlab.com/nael_d/encrypia/-/archive/master/encrypia-master.zip) manually from GitLab.

## Getting started
Encrypia requires you to set a custom key using `setKey()`:
```php
Encrypia::setKey(1234567890);
```
> 💡 For enhanced security, we recommend using a key with at least 8 characters.

Encrypia employs the `blind()` method for encryption and the `unblind()` method for decryption. A unique encryption key is required for secure data handling.

```php
// Encrypt a message
Encrypia::setKey(6537791);
$encryptedText = Encrypia::blind('Hello world!');
echo $encryptedText;
// output: Y8b6H3D6KPKw0yjH37Gk6276TIYPEpIktNrIKkKw5Sk=

// Decrypt the message
$decryptedText = Encrypia::unblind($encryptedText);
echo $decryptedText;
// Output: Hello world!
```

Encrypia offers flexibility by allowing you to specify a custom encryption key for individual operations, while maintaining the global key for consistent encryption and decryption.

```php
Encrypia::setKey(6537791); // global key
echo Encrypia::blind('Hello world!', 991340785); // passing custom key
// output: 1vumtX0sQsauiWV3pUfdt3ChbbRG6UzLIhw+bhU9KTY=

echo Encrypia::unblind('1vumtX0sQsauiWV3pUfdt3ChbbRG6UzLIhw+bhU9KTY=', 991340785);
// output: Hello world!
```

## Advanced Settings
Encrypia comes up with adnvaned settings using `settings()` with this options:
- **ENC_COMPRESS**: compresses texts to reduce size for efficient storing.
  ```php
  Encrypia::settings(Encrypia::ENC_COMPRESS);
  ```

- **ENC_SECURE_KEY**: encrypts the secret key for more encryption level.
  ```php
  Encrypia::settings(Encrypia::ENC_SECURE_KEY);
  ```

You may combine multiple settings using pipepline `|`:
```php
Encrypia::settings(
  Encrypia::ENC_COMPRESS | Encrypia::ENC_SECURE_KEY
);
```

## Conditions of usage
Encrypia is free to use in all your projects, Commercial and Personal.

