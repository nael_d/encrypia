<?php

class Encrypia {
  private static $cipher = 'AES-256-CBC'; // Encryption cipher
  private static $key; // Encryption key
  private static $ivLength; // Initialization Vector length
  private static $useCompression = false; // Default: Do not compress
  private static $useSecureKey = false; // Default: Do not use secure key generation
  private static $ENC_OPTIONS = [];

  // Define constants for settings
  const ENC_COMPRESS    = 0x01;
  const ENC_SECURE_KEY  = 0x02;

  // check php version compatibility
  private static function check() {
    if (version_compare(PHP_VERSION, '7.0.0','<')) {
      throw new Exception('Encrypia requires at least PHP >= 7.0.0');
    }
  }

  // Optional settings method
  public static function settings($options) {
    // check php version compatiblity
    self::check();

    // Check for ENC_COMPRESS setting
    if ($options & self::ENC_COMPRESS) {
      self::$useCompression = true;
    }

    // Check for ENC_SECURE_KEY setting
    if ($options & self::ENC_SECURE_KEY) {
      self::$useSecureKey = true;
    }
  }

  private static function setIvLength() {
    // check php version compatiblity
    self::check();

    self::$ivLength = openssl_cipher_iv_length(self::$cipher); // Get IV length based on cipher
  }

  // Set the encryption key
  public static function setKey($key) {
    $key = self::$useSecureKey ? self::generateSecureKey($key) : $key;
    self::$key = hash('sha256', $key, true);
    self::setIvLength();
  }

  // Get the currently set encryption key
  public static function getKey() {
    return self::$key;
  }

  // Generate a secure key using the chess-based method
  private static function generateSecureKey($key) {
    // Split the key into pairs of digits
    $keyParts = str_split($key, 2);
    $newKey = '';

    foreach ($keyParts as $index => $part) {
      // Convert part to an integer and reduce it to fit within the chessboard (1-64)
      $position = (int) $part % 64;
      $position = $position === 0 ? 64 : $position; // Ensure it's between 1 and 64

      // Get available moves for the bishop from this position
      $moves = self::getKnightMoves($position);

      // Choose the move based on the part's index in the key
      if (count($moves) >= $index + 1) {
        $chosenMove = $moves[$index];
      }
      else {
        // If not enough moves, sum available moves and divide by the current index + 1
        $sumMoves = array_sum($moves);
        $chosenMove = intdiv($sumMoves, $index + 1);
      }

      // Append the chosen move to the new key
      $newKey .= $chosenMove;
    }

    // Hash the newly generated key
    return $newKey; //hash('sha256', $newKey, true);
  }

  // Get possible moves for a knight on a given position (1-64)
  private static function getKnightMoves($position) {
    // Adjust the numbering to match a chessboard starting from the right-bottom (h1 = 1) to top-left (a8 = 64)
    $col = ($position - 1) % 8;
    $row = intdiv($position - 1, 8);

    $moves = [];

    // Define possible knight moves (row offset, col offset)
    $knightMoves = [
      [2, 1], [2, -1], [-2, 1], [-2, -1], // Moves that are 2 squares vertically and 1 horizontally
      [1, 2], [1, -2], [-1, 2], [-1, -2]  // Moves that are 1 square vertically and 2 horizontally
    ];

    foreach ($knightMoves as $move) {
      $newRow = $row + $move[0];
      $newCol = $col + $move[1];

      // Ensure the move stays within the bounds of the chessboard
      if ($newRow >= 0 && $newRow < 8 && $newCol >= 0 && $newCol < 8) {
        $newPosition = $newRow * 8 + $newCol + 1;
        $moves[] = $newPosition;
      }
    }

    return $moves;
  }

  // Encrypt the text
  public static function blind($text, $key = null) {
    if ($key) {
      // Use provided key if given, otherwise use the globally set key.
      $key = self::$useSecureKey ? self::generateSecureKey($key) : $key;
    }

    self::setIvLength();
    $encryptionKey = $key ? hash('sha256', $key, true) : self::$key;

    if (!$encryptionKey) {
      throw new Exception('Encryption key is not set.');
    }

    $iv = openssl_random_pseudo_bytes(self::$ivLength); // Generate a random IV
    $ciphertext = openssl_encrypt($text, self::$cipher, $encryptionKey, OPENSSL_RAW_DATA, $iv);

    if ($ciphertext === false) return;

    // Return the IV and the ciphertext together (IV is needed for decryption)
    return base64_encode($iv . $ciphertext);
  }

  // Decrypt the text
  public static function unblind($text, $key = null) {
    if ($key) {
      // Use provided key if given, otherwise use the globally set key.
      $key = self::$useSecureKey ? self::generateSecureKey($key) : $key;
    }

    self::setIvLength();
    $encryptionKey = $key ? hash('sha256', $key, true) : self::$key;

    if (!$encryptionKey) {
      throw new Exception('Decryption key is not set.');
    }

    $ciphertext = base64_decode($text); // Decode the base64 encoded string
    $iv = substr($ciphertext, 0, self::$ivLength); // Extract the IV
    $actualCiphertext = substr($ciphertext, self::$ivLength); // Extract the ciphertext

    $plaintext = openssl_decrypt($actualCiphertext, self::$cipher, $encryptionKey, OPENSSL_RAW_DATA, $iv);

    if ($ciphertext === false) return;

    return $plaintext;
  }
}
